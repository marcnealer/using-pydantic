# A Guide to using Pydantic

When I started to experiment with FastAPI, I came across Pydantic' and I have to tell the truth, it confused the
hell out of me. With that said, the more I used the library, the more I liked it. I also saw the reasons why 
Pydantic looks and feels confusing to many. Thus, this is a guide to using Pydantic in a specific way, ignoring
the multiple ways that it can be set-up to do the same thing.

> This document discusses Pydantic v2.* not v1. There are significant differences.
> If your using django Ninja, be aware that v1 uses Pydantic v2.6 or above. Previous versions use Pydantic v1

## So what is Pydantic

Pydantic is Python Dataclasses, with Validation and other extras. In a simpler explanation, it allows you
to take input data, shape it into a dataclass for easy use, while at the same time making sure the data that
was sent is correct. 

So if you are expecting 
```python
first_name = "marc" # a string
last_name = "nealer" # a string
```
Then you want to make sure you are sent two parameters that are of a set name and a string. A Pydantic model (class)
for this looks like this

```python
from pydantic import BaseModel

class MyFirstModel(BaseModel):
    first_name: str
    last_name: str

validating = MyFirstModel(first_name="marc", last_name="nealer")
```

This is a really simple example, but you can see that you can get rid of all the manual validations etc. If your
a Django coder, you might see the similarity to Forms.

## Lets get a little more complicated

Inputs are often optional, or can be of different types. Well no problem

```python
from pydantic import BaseModel
from typing import Union, Optional

class MySecondModel(BaseModel):
    first_name: str
    middle_name: Union[str, None] # This means the parameter doesn't have to be sent
    title: Optional[str] # this means the parameter should be sent, but can be None
    last_name: str
```

So what is the difference between "middle_name" and "title". Well, Pydantic will see middle name as a totally optional
parameter that does not have to be present in the sent data. With "title", Pydantic will want to see the parameter
being sent and will complain if it's not there, but it will accept a None value. 

Yep, a little confusing, but there you go.

This is all considered the default validation part of Pydantic. We can use basic types and the typing module to 
do some complex validation though

```python
from pydantic import BaseModel
from typing import Union, List, Dict
from datetime import datetime

class MyThirdModel(BaseModel):
    name: Dict[str: str]
    skills: List[str]
    holidays: List[Union[str, datetime]]
```

## Applying default values

There is a funny querk of Pydantic when it comes to applying default values for missing parameters. Take the following

```python
from pydantic import BaseModel


class DefaultsModel(BaseModel):
    first_name: str = "jane"
    middle_names: list = []
    last_name : str = "doe"
```

Your thinking that looks all ok. Here's the string thing. With "middle_name", it doesn't apply a blank list to each
object created, but applys the SAME blank list to all of them. Thus if you create two objects and update "middle_name"
on one, both are updated.

To resolve this, we need to introduce the "Field" object

```python
from pydantic import BaseModel, Field

class DefaultsModel(BaseModel):
    first_name: str = "jane"
    middle_names: list = Field(default_factory=list)
    last_name: str = "doe"
```
With this Pydantic uses the function passed to the default_factory parameter to create a unique object each time. In 
the Pydantic documentation, a largish chunk of space is given over to the different uses for the Field object, but 
when you get into more complex stuff, the Field object doesn't really work that well alot of the time. As such, I am
only going to show this one use for it. Take a look at the Docs if you want to see more, but be warned, it gets confusing
especially when the other items that you can use Field for, can also be done using other methods.

## Custom Validation

The default validation works ok, if all you want to do is check the types, but most of the time we want to take
things a little further. There are a lot more ways to apply more complex validations than the one I'm showing here,
but I think this works out the best and I think the simplest.

### Field validation, Before and After

Before we move on, I should explain all the places we can apply validation. Lets start with Before and After.
This is where we apply validation either Before the default, or After. This can be very useful. Lets say
your incoming data has a date in a string field. You can take this value, check its actually a date and convert it
to a datetime object, before the default, which is expecting a datetime.

```python
from pydantic import BaseModel, BeforeValidator, ValidationError
import datetime
from typing import Annotated


def stamp2date(value):
    if not isinstance(value, float):
        raise ValidationError("incoming date must be a timestamp")
    try:
        res = datetime.datetime.fromtimestamp(value)
    except ValueError:
        raise ValidationError("Time stamp appears to be invalid")
    return res


class DateModel(BaseModel):
    dob: Annotated[datetime.datetime, BeforeValidator(stamp2date)]
```
To me, this format looks neat and tidy and makes sense. You can apply custom validators through the Field object,
or by using decorators, but I recommend this one.

if you want the value to be optional, then your code would look like this

```python
from pydantic import BaseModel, BeforeValidator, ValidationError, Field
import datetime
from typing import Annotated


def stamp2date(value):
    if not isinstance(value, float):
        raise ValidationError("incoming date must be a timestamp")
    try:
        res = datetime.datetime.fromtimestamp(value)
    except ValueError:
        raise ValidationError("Time stamp appears to be invalid")
    return res


class DateModel(BaseModel):
    dob: Annotated[Annotated[datetime.datetime, BeforeValidator(stamp2date)] | None, Field(default=None)]
```
As I mentioned, you can have multiple validators before and after. The validators will be run in order, where it will
go through and run the BeforeValidators in the order they are specified. Then run the AfterValidators (after the default
validation).

### Model validation, Before and After

Model Validation can be very useful. Lets say you have 5 parameters that are all optional, but at least one must be
there. 


```python
from pydantic import BaseModel, model_validator, ValidationError
from typing import Union, Any

class AllOptionalAfterModel(BaseModel):
    param1: Union[str, None] = None
    param2: Union[str, None] = None
    param3: Union[str, None] = None
    
    @model_validator(mode="after")
    def there_must_be_one(self):
        if not (self.param1 or self.param2 or self.param3):
            raise ValidationError("One parameter must be specified")
        return self

class AllOptionalBeforeModel(BaseModel):
    param1: Union[str, None] = None
    param2: Union[str, None] = None
    param3: Union[str, None] = None
    
    @model_validator(mode="before")
    @classmethod
    def there_must_be_one(cls, data: Any):
        if not (data["param1"] or data["param2"] or data["param3"]):
            raise ValidationError("One parameter must be specified")
        return data
```
So a couple of differences between Before and After. Before is passed the class and the list of parameters and runs
as a class method. That is, its run before the object is initialized. After is run after all the over validators have
completed. Do note that the Before MUST have decorators for model_validator and classmethod, in this order.


## Alias's

Alias's actually do two different jobs. They can be used to alter the names of fields when the object is serialized.
The other option is to point towards an alternative field or parameter when the data comes in. I use the latter most
of the time, as I have to deal with data that comes in from various sources and I prefer to have standardized names.

Like with everything else in Pydantic there are multiple ways to define Alias's, the main one is defining them in using
the Field Object. The problem with using the field object is that it gets in the way of other Field object functions,
thus I find it preferable to use the alternative method, that of the AliasGenerator.

### AliasGenerator

The AliasGenerator, is an object that can be passed to the config of your base model. Here is an example that
I have taken directly from the documentation

```python
from pydantic import AliasGenerator, BaseModel, ConfigDict


class Tree(BaseModel):
    model_config = ConfigDict(
        alias_generator=AliasGenerator(
            validation_alias=lambda field_name: field_name.upper(),
            serialization_alias=lambda field_name: field_name.title(),
        )
    )

    age: int
    height: float
    kind: str


t = Tree.model_validate({'AGE': 12, 'HEIGHT': 1.2, 'KIND': 'oak'})
print(t.model_dump(by_alias=True))
#> {'Age': 12, 'Height': 1.2, 'Kind': 'oak'}
```
Notice the two parameters passed. "validation_alias" and "serialization_alias". So "validation_alias" is used to 
define alternative parameter names that could be present in the incoming data. "serialization_alias" sets alternative
names that can be used when serializing the data, though, you do need to request that the alias's be used when you
do so.

The above example isn't really of much use, but adding in AliasPath and AliasChoices objects, and we get a powerful
feature

### AliasChoices

AliasChoices, allows us to pass a list of alternative names, that could hold the data we want in a given field

```python
from pydantic import BaseModel, ConfigDict, AliasGenerator, AliasChoices

aliases = {
    "first_name": AliasChoices("fname", "surname", "forename", "first_name"),
    "last_name": AliasChoices("lname", "family_name", "last_name")
}


class FirstNameChoices(BaseModel):
    model_config = ConfigDict(
        alias_generator=AliasGenerator(
            validation_alias=lambda field_name: aliases.get(field_name, None)
        )
    )
    title: str
    first_name: str
    last_name: str

```

Yeah, a little complex, but basically the lambda takes the field name and pulls the relevant AliasChoices object 
from the dictionary. Pydantic will then look through the incoming data for a match and pass that value into the field.
So as the name suggests, it gives a choice of possible input parameter names that can be matched to a field.

### AliasPath

AliasPath is used to define a pathway to where the required value is

```python
from pydantic import BaseModel, ConfigDict, AliasGenerator, AliasPath

aliases = {
    "first_name": AliasPath("name", "first_name"),
    "last_name": AliasPath("name",  "last_name")
}


class FirstNameChoices(BaseModel):
    model_config = ConfigDict(
        alias_generator=AliasGenerator(
            validation_alias=lambda field_name: aliases.get(field_name, None)
        )
    )
    title: str
    first_name: str
    last_name: str

obj = FirstNameChoices(**{"name":{"first_name": "marc", "last_name": "Nealer"},"title":"Master Of All"})
```
### Using AliasPath and AliasChoices

We can use both together

```python
from pydantic import BaseModel, ConfigDict, AliasGenerator, AliasPath, AliasChoices

aliases = {
    "first_name": AliasChoices("first_name", AliasPath("name", "first_name")),
    "last_name": AliasChoices("last_name", AliasPath("name",  "last_name"))
}


class FirstNameChoices(BaseModel):
    model_config = ConfigDict(
        alias_generator=AliasGenerator(
            validation_alias=lambda field_name: aliases.get(field_name, None)
        )
    )
    title: str
    first_name: str
    last_name: str

obj = FirstNameChoices(**{"name":{"first_name": "marc", "last_name": "Nealer"},"title":"Master Of All"})
```
This gives a great way to flatten/reformat your data as it comes in 

## Final Thoughts

Pydantic is a very powerful tool once you understand it. The paths I've shown above, should cover the majority of
your potential usecases for it, without the documentation tell you half a dozen ways of doing the same thing. There
are a lot of other things that Pydantic can do, but I think these are the best and mose useful.

